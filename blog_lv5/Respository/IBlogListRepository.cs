﻿using blog_lv5.Models.DTO;

namespace blog_lv5.Respository
{
    public interface IBlogListRepository
    {
        IEnumerable<Object> GetAllBlogs();
        Object GetBlogById(int id);
        Object CreateBlog(BlogListDTO blogListDTO);
        Object EditBlogById(int id, BlogListDTO blogListDTO);
        Object DeleteBlogById(int id);
        void Save();

        /*IEnumerable<Student> GetStudents();
        Student GetStudentByID(int studentId);
        void InsertStudent(Student student);
        void DeleteStudent(int studentID);
        void UpdateStudent(Student student);
        void Save();*/
    }
}
