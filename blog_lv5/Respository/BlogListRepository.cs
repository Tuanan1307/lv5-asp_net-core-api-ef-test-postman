﻿using blog_lv5.Data;
using blog_lv5.Models;
using blog_lv5.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace blog_lv5.Respository
{
    public class BlogListRepository : IBlogListRepository
    {
        private readonly BlogListDBContext _context;
        private static readonly List<string> LISTPOS = new List<string>(new[] { "Việt Nam", "Châu Á", "Châu Âu", "Châu Mỹ" });
        public BlogListRepository(BlogListDBContext context)
        {
            _context = context;
        }
        //CreateBlog called Blogcontroller PostPosition() / CreateBlog(blogDto)
        //return blogcreated
        public object CreateBlog(BlogListDTO blogListDTO)
        {
            List<int> listposition = blogListDTO.listposition.Split(',').Select(Int32.Parse).ToList();
            BlogList blogList = new BlogList()
            {
                Id = blogListDTO.Id,
                title = blogListDTO.title,
                description = blogListDTO.description,
                detail = blogListDTO.detail,
                thumb = blogListDTO.thumb,
                data_Public = blogListDTO.data_Public,
                pubblic = blogListDTO.pubblic,
                CategoryId = blogListDTO.CategoryId,
                category = null,
                Positions = null,
            };
            _context.BlogLists.Add(blogList);
            _context.SaveChanges();
            foreach (var id in listposition)
            {
                if (id < LISTPOS.Count)
                {
                    Position position = new Position()
                    {
                        Id = 0,
                        PositionName = LISTPOS[id - 1].ToString(),
                        BlogListId = blogList.Id
                    };
                    _context.Positions.Add(position);
                }
            }
            _context.SaveChanges();
            return blogList;
        }


        //Delete blog called Blogcontroller DeleteBlog(id)
        //return blog
        public object DeleteBlogById(int id)
        {
            BlogList blogList = _context.BlogLists.Find(id);
            if (blogList == null)
            {
                return null;
            }
            _context.Positions.RemoveRange(_context.Positions.Where(x => x.BlogListId == id).ToList());
            _context.BlogLists.Remove(blogList);
            _context.SaveChanges();
            return blogList;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        //Edit blog called Blogcontroller PutBlog(id,blogDTO)
        //return blog
        public object EditBlogById(int id, BlogListDTO blogListDTO)
        {
            List<int> listposition = blogListDTO.listposition.Split(',').Select(Int32.Parse).ToList();
            if (id != blogListDTO.Id)
            {
                return null;
            }
            BlogList blogList = new BlogList()
            {
                Id = blogListDTO.Id,
                title = blogListDTO.title,
                description = blogListDTO.description,
                detail = blogListDTO.detail,
                thumb = blogListDTO.thumb,
                data_Public = blogListDTO.data_Public,
                pubblic = blogListDTO.pubblic,
                CategoryId = blogListDTO.CategoryId,
                category = null,
                Positions = null,
            };
            _context.Entry(blogList).State = EntityState.Modified;
            _context.Positions.RemoveRange(_context.Positions.Where(x => x.BlogListId == id).ToList());
            foreach (var item in listposition)
            {
                Position position = new Position()
                {
                    Id = 0,
                    PositionName = LISTPOS[item - 1].ToString(),
                    BlogListId = blogList.Id
                };
                _context.Positions.Add(position);
            }
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.BlogLists.Find(id) == null)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
            return blogList;

        }
        //Get all blog called Blogcontroller GetBlog()
        //return list blog
        public IEnumerable<object> GetAllBlogs()
        {
            if (_context.BlogLists == null)
            {
                return null;
            }
            return _context.BlogLists.Include(x => x.category).Include(x => x.Positions).ToList();
        }
        //Get blog by id called Blogcontroller GetBlog(id)
        //return blog
        public object GetBlogById(int id)
        {
            if (_context.BlogLists == null)
            {
                return null;
            }
            return _context.BlogLists.Where(x => x.Id == id).Include(x => x.category).Include(x => x.Positions).First();
        }
        //save changes in database
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
