﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace blog_lv5.Migrations
{
    public partial class bloglistDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categorys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BlogLists",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    detail = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    thumb = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    data_Public = table.Column<DateTime>(type: "datetime2", nullable: false),
                    pubblic = table.Column<bool>(type: "bit", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlogLists_Categorys_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categorys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Positions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PositionName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BlogListId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Positions_BlogLists_BlogListId",
                        column: x => x.BlogListId,
                        principalTable: "BlogLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categorys",
                columns: new[] { "Id", "CategoryName" },
                values: new object[,]
                {
                    { 1, "Kinh doanh" },
                    { 2, "Giải trí" },
                    { 3, "Thế giới" },
                    { 4, "Thời sự" },
                    { 5, "Y tế" },
                    { 6, "ShowBizz" },
                    { 7, "Thể thao" }
                });

            migrationBuilder.InsertData(
                table: "BlogLists",
                columns: new[] { "Id", "CategoryId", "data_Public", "description", "detail", "pubblic", "thumb", "title" },
                values: new object[] { 1, 2, new DateTime(2022, 8, 15, 17, 0, 25, 116, DateTimeKind.Local).AddTicks(3227), "Việt Nam", "ddđ", true, "dssđ", "dada" });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Id", "BlogListId", "PositionName" },
                values: new object[,]
                {
                    { 1, 1, "Việt Nam" },
                    { 2, 1, "Châu Á" },
                    { 3, 1, "Châu Âu" },
                    { 4, 1, "Châu Mỹ" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlogLists_CategoryId",
                table: "BlogLists",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Positions_BlogListId",
                table: "Positions",
                column: "BlogListId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Positions");

            migrationBuilder.DropTable(
                name: "BlogLists");

            migrationBuilder.DropTable(
                name: "Categorys");
        }
    }
}
