﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using blog_lv5.Data;

#nullable disable

namespace blog_lv5.Migrations
{
    [DbContext(typeof(BlogListDBContext))]
    partial class BlogListDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("blog_lv5.Models.BlogList", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("CategoryId")
                        .HasColumnType("int");

                    b.Property<DateTime>("data_Public")
                        .HasColumnType("datetime2");

                    b.Property<string>("description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("detail")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("pubblic")
                        .HasColumnType("bit");

                    b.Property<string>("thumb")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.ToTable("BlogLists");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CategoryId = 2,
                            data_Public = new DateTime(2022, 8, 15, 17, 0, 25, 116, DateTimeKind.Local).AddTicks(3227),
                            description = "Việt Nam",
                            detail = "ddđ",
                            pubblic = true,
                            thumb = "dssđ",
                            title = "dada"
                        });
                });

            modelBuilder.Entity("blog_lv5.Models.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("CategoryName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Categorys");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CategoryName = "Kinh doanh"
                        },
                        new
                        {
                            Id = 2,
                            CategoryName = "Giải trí"
                        },
                        new
                        {
                            Id = 3,
                            CategoryName = "Thế giới"
                        },
                        new
                        {
                            Id = 4,
                            CategoryName = "Thời sự"
                        },
                        new
                        {
                            Id = 5,
                            CategoryName = "Y tế"
                        },
                        new
                        {
                            Id = 6,
                            CategoryName = "ShowBizz"
                        },
                        new
                        {
                            Id = 7,
                            CategoryName = "Thể thao"
                        });
                });

            modelBuilder.Entity("blog_lv5.Models.Position", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("BlogListId")
                        .HasColumnType("int");

                    b.Property<string>("PositionName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("BlogListId");

                    b.ToTable("Positions");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            BlogListId = 1,
                            PositionName = "Việt Nam"
                        },
                        new
                        {
                            Id = 2,
                            BlogListId = 1,
                            PositionName = "Châu Á"
                        },
                        new
                        {
                            Id = 3,
                            BlogListId = 1,
                            PositionName = "Châu Âu"
                        },
                        new
                        {
                            Id = 4,
                            BlogListId = 1,
                            PositionName = "Châu Mỹ"
                        });
                });

            modelBuilder.Entity("blog_lv5.Models.BlogList", b =>
                {
                    b.HasOne("blog_lv5.Models.Category", "category")
                        .WithMany()
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("category");
                });

            modelBuilder.Entity("blog_lv5.Models.Position", b =>
                {
                    b.HasOne("blog_lv5.Models.BlogList", null)
                        .WithMany("Positions")
                        .HasForeignKey("BlogListId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("blog_lv5.Models.BlogList", b =>
                {
                    b.Navigation("Positions");
                });
#pragma warning restore 612, 618
        }
    }
}
