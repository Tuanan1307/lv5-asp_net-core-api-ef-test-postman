﻿using blog_lv5.Data;
using blog_lv5.Models;
using blog_lv5.Models.DTO;
using blog_lv5.Respository;
using Microsoft.AspNetCore.Mvc;

namespace blog_lv5.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogListsController : ControllerBase
    {

        private readonly BlogListDBContext _context;
        private BlogListRepository _repository;
        private IBlogListRepository blogRepository;

        public BlogListsController(BlogListDBContext context)
        {
            _repository = new BlogListRepository(context);
            blogRepository = new BlogListRepository(context);
            _context = context;
        }

        // GET: api/Blogs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Object>>> GetBlogs()
        {
            if (_context.BlogLists == null)
            {
                return NotFound();
            }
            IEnumerable<BlogList> blogLists = (IEnumerable<BlogList>)_repository.GetAllBlogs();
            return blogLists.ToList();
        }

        // GET: api/Blogs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Object>> GetBlog(int id)
        {
            if (_context.BlogLists == null)
            {
                return NotFound();
            }

            BlogList blog = (BlogList)_repository.GetBlogById(id);
            /*            Blog blog1 = (Blog)blogRepository.DeleteBlogById(id);*/

            /*var test = new
            { 
                id = blog.Id,
                title = blog.title,
                detail = blog.detail,
                pos = blog.Positions,
                check = "asdasd"
            };*/
            if (blog == null)
            {
                return NotFound();
            }
            return Accepted(blog);
        }

        // PUT: api/Blogs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBlog(int id, BlogListDTO blogListDTO)
        {
            if (id != blogListDTO.Id)
            {
                return BadRequest();
            }
            BlogList blog = (BlogList)_repository.EditBlogById(id, blogListDTO);
            if (blog == null)
            {
                return NotFound();
            }
            return Accepted("Edited!");
        }

        // POST: api/Blogs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BlogList>> PostBlog(BlogListDTO blogListDTO)
        {
            if (_context.BlogLists == null)
            {
                return Problem("Entity set 'BlogContext.Blogs'  is null.");
            }
            BlogList blogList = (BlogList)_repository.CreateBlog(blogListDTO);

            return CreatedAtAction("GetBlog", new { id = blogList.Id }, blogList);
        }

        // DELETE: api/Blogs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBlog(int id)
        {
            if (_context.BlogLists == null)
            {
                return NotFound();
            }
            var blog = await _context.BlogLists.FindAsync(id);
            if (blog == null)
            {
                return NotFound();
            }
            BlogList blogdeleted = (BlogList)_repository.DeleteBlogById(id);
            if (blogdeleted == null)
            {
                return NotFound();
            }
            return Accepted("deleted!");

        }



        private bool BlogListExists(int id)
        {
            return (_context.BlogLists?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
