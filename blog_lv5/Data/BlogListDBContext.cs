﻿using blog_lv5.Models;
using Microsoft.EntityFrameworkCore;

namespace blog_lv5.Data
{
    public class BlogListDBContext : DbContext
    {
        public BlogListDBContext(DbContextOptions<BlogListDBContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlogList>().HasData(
                   new BlogList() { Id = 1, title = "dada", description = "Việt Nam", detail = "ddđ", CategoryId = 2, pubblic = true, thumb = "dssđ", data_Public = DateTime.Now, }

               );
            modelBuilder.Entity<Position>().HasData(
                    new Position() { Id = 1, PositionName = "Việt Nam", BlogListId = 1 },
                     new Position() { Id = 2, PositionName = "Châu Á", BlogListId = 1 },
                      new Position() { Id = 3, PositionName = "Châu Âu", BlogListId = 1 },
                       new Position() { Id = 4, PositionName = "Châu Mỹ", BlogListId = 1 }
                );
            modelBuilder.Entity<Category>().HasData(
                   new Category() { Id = 1, CategoryName = "Kinh doanh" },
                    new Category() { Id = 2, CategoryName = "Giải trí" },
                     new Category() { Id = 3, CategoryName = "Thế giới" },
                      new Category() { Id = 4, CategoryName = "Thời sự" },
                         new Category() { Id = 5, CategoryName = "Y tế" },
                    new Category() { Id = 6, CategoryName = "ShowBizz" },
                     new Category() { Id = 7, CategoryName = "Thể thao" }

               );
            //modelBuilder.Entity<BlogList>()
            // .HasOne<Position>(b => b.Position)
            // .WithMany(p => p.BlogLists)
            // .HasForeignKey(s => s.PositionId);

            //modelBuilder.Entity<BlogList>()
            // .HasOne<Category>(b => b.Category)
            // .WithMany(p => p.BlogLists)
            // .HasForeignKey(s => s.CategoryId);
        }

        public DbSet<BlogList> BlogLists { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<Category> Categorys { get; set; }
    }
}
