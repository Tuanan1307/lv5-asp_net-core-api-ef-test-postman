﻿namespace blog_lv5.Models.DTO
{
    public class BlogListDTO
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string detail { get; set; }
        public string? thumb { get; set; }
        public DateTime data_Public { get; set; }
        public bool pubblic { get; set; }
        public int CategoryId { get; set; }
        public string listposition { get; set; }
    }
}
