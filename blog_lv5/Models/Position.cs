﻿namespace blog_lv5.Models
{
    public class Position
    {

        public int Id { get; set; }

        public string PositionName { get; set; }

        public int BlogListId { get; set; }
        //public virtual ICollection<BlogList> BlogLists { get; set; }

        //public Position()
        //{
        //    BlogLists = new List<BlogList>();
        //}
    }
}
