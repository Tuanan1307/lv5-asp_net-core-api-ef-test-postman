﻿namespace blog_lv5.Models
{
    public class Category
    {

        public int Id { get; set; }

        public string CategoryName { get; set; }

        //public virtual ICollection<BlogList> BlogLists { get; set; }

        //public Category()
        //{
        //    BlogLists = new List<BlogList>();
        //}
    }
}
