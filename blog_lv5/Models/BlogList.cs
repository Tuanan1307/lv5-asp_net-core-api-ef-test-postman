﻿using System.ComponentModel.DataAnnotations;

namespace blog_lv5.Models
{
    public class BlogList
    {
        [Key]
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string detail { get; set; }
        public string? thumb { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime data_Public { get; set; }
        public bool pubblic { get; set; }


        public int CategoryId { get; set; }
        public virtual Category category { get; set; }
        public virtual ICollection<Position> Positions { get; set; }

    }





}
